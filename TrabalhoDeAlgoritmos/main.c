//
//  main.c
//  TrabalhoDeAlgoritmos
//
//  Created by Aitor Lucas on 22/06/17.
//  Copyright AitorLucasTeam© 2017. All rights reserved.
//


//  MARK: Bibliotecas Utilizadas
#include <stdio.h>
//#include <stdlib.h>
#include <math.h>


//  MARK: Defines
// -> Define valores para verdadeiro e falso (bool) e um valor maximo para ser usado no vetor resposta;
#define Verdadeiro 1
#define Falso      0
#define MAX        50

// -> Define possibilidades do menu;
#define Problema1 1
#define Problema2 2
#define Deflexao  3
#define Sair      4


//  MARK: Variaveis globais utilizadas.
double P = 0.0, E = 0.0, I = 0.0, L = 0.0;


//  MARK: Funções utilizidas
// -> Funções suporte (Declaração);
void   imprime_menu();
void   imprime_resultado();
double retorna_valor_digitado(char nome[]);
double calcula_h(double a, double b, double m);
void   resolve_problema(double a, double b, double y1a, double y2a, double m);

// -> Ponteiros da função;
// Utilizados nesse caso para permetir o uso das funçoes que foram passadas na descriçao do trabalho.
double (*f1)(double, double, double);
double (*f2)(double, double, double);

// -> Funções definidas pela descrição do trabalho
double funcao1_problema1(double x, double y1, double y2) {
    return y2;
}

double funcao2_problema1(double x, double y1, double y2) {
    return 2*y1 + y2 - x*x;
}

double funcao1_problema2(double x, double y1, double y2) {
    return y1 + y2 + 3*x;
}

double funcao2_problema2(double x, double y1, double y2) {
    return 2*y1 - y2 - x;
}

double funcao1_problema3(double x, double y1, double y2) {
    return y2;
}

double funcao2_problema3(double x, double y1, double y2) {
    return (P/E*I)*(pow((1+(y2)*(y2)), 1.5))*(L-x);
}


// -> Main;
int main() {
    
// Variáveis
    int repete = Verdadeiro, opcao;
    double a = 0.0, b = 0.0, y1a = 0.0, y2a = 0.0, m = 0.0;
    
// Repetição até o usuario escolher sair do programa.
    while (repete == Verdadeiro) {
        imprime_menu();
        
// Escolha do usuário.
        scanf("%d", &opcao); /*limpar*/
        
// Validação se o usuario deseja sair do programa.
        if (opcao == Sair) {
            repete = Falso; //Desnecessário, serve apenas para entendimento.
            printf("\n\n\t FIM !!\n\n");
            return 0;
       
            
// Validação se o usuario digitou um valor válido.
//  MARK: Problemas a serem solucionados
        } else if ((opcao == Problema1) || (opcao == Problema2) || (opcao == Deflexao))  {
            /*limpar*/
            printf("\n");
            
            if (opcao != Deflexao) {
                a  = retorna_valor_digitado("a");
                b  = retorna_valor_digitado("b");
                y1a = retorna_valor_digitado("y1a");
                y2a = retorna_valor_digitado("y2a");
                m  = retorna_valor_digitado("m");
                
                switch (opcao) {
// -> Problema 1;
//    y1′ = f1(x,y1,y2) = y2
//    y2′ = f2(x,y1,y2) = 2y1 + y2 − xˆ2
//    y1(0.0) = 1.0
//    y2(0.0) = 0.0
                    case Problema1:
                        f1 = &funcao1_problema1;
                        f2 = &funcao2_problema1;
                        break;
// -> Problema 2;
//    y1′ = f1(x,y1,y2) = y1 + y2 + 3x
//    y2′ = f2(x,y1,y2) = 2y1 − y2 − x
//    y1(0.0) = 1.0
//    y2(0.0) = −1.0
                    case Problema2:
                        f1 = &funcao1_problema2;
                        f2 = &funcao2_problema2;
                        break;
                }
            } else {
// -> Deflexão da viga;
//    y′′ = (P/EI) * ((1 + (y′)ˆ2)ˆ1.5) * (L−x)
//    y(0) = 0
//    y′(0) = 0
                P = retorna_valor_digitado("P");
                E = retorna_valor_digitado("E");
                I = retorna_valor_digitado("I");
                L = retorna_valor_digitado("L");
                m  = retorna_valor_digitado("m");

// Adequação das variaveis do problema de deflexão para a funcão geral utilizada,
// que recebe, como parametro, valores de a, b, y1a, y2a e m.
                a = 0;
                b = L;
                y1a = 0;
                y2a = 0;
                
                f1 = &funcao1_problema3;
                f2 = &funcao2_problema3;
            }
            
            resolve_problema(a, b, y1a, y2a, m);
            printf("\n\n Obs.: É permitido no máximo 50 subdivisões, alem disso, os resultados são impressos com precisao de 5 casas decimais.");
            printf("\nCaso queira, altere no codigo essas restricoes.\n\n");
        } else {
// Valor inválido
            /*limpar*/
            printf("\nDigite algum valor válido.\n");
            /*pausar*/
        }
    }
// Ocorreu algum erro inesperado
    printf("\n\nERROR!!\n");
    return 1;
}


//  MARK: Funções suporte
// -> Imprimir menu;
void imprime_menu() {
    printf("\nDigite uma opção:\n");
    printf("\n 1 - Resolver o problema 1 (o exemplo da validação).");
    printf("\n 2 - Resolver o problema 2.");
    printf("\n 3 - Resolver o problema da deflexão da viga.");
    printf("\n 4 - Sair");
    printf("\n\nEscolha: ");
}

// -> Imprime os resultados obtidos;
void imprime_resultado(double x, double y1, double y2) {
    printf("\n x = %.5f,\ty1 = %.5f\t\ty2 = %.5f", x, y1, y2);
}

// -> Retorna valor digitado pelo usuario;
double retorna_valor_digitado(char nome[]) {
    double numero_digitado;
    
    printf("Digite um valor para %s: ", nome); /*limpar e escrever*/
    scanf("%lf", &numero_digitado);
    return numero_digitado;
}

// -> Retorna valor para h;
double calcula_h(double a, double b, double m) {
    return ((b-a)/m);
}

// -> Resolve as EDOs proposta.
void resolve_problema(double a, double b, double y1a, double y2a, double m) {
    double k11, k12, k21, k22, k31, k32, k41, k42;
// Atribuição do valor de h (calculado) e do valor inical do problema
    double h = calcula_h(a, b, m), x = a;
    
// Vetores resposta/resolução
    double y1[MAX], y2[MAX];
   
// Valor inicial
    y1[0] = y1a;
    y2[0] = y2a;
   
// Imprime o valor inicial (como descrito no trabalho)
    imprime_resultado(x, y1[0], y2[0]);
    
    for(int n = 0; n < m; n++) {
// Cálculo das inclinações
//        k = f(x, y1, y2)
        k11 = f1(x, y1[n], y2[n]);
        k12 = f2(x, y1[n], y2[n]);
        k21 = f1((x + (h/2)), (y1[n] + (h/2)*(k11)), (y2[n] + (h/2)*k12));
        k22 = f2((x + (h/2)), (y1[n] + (h/2)*(k11)), (y2[n] + (h/2)*k12));
        k31 = f1((x + (h/2)), (y1[n] + (h/2)*(k21)), (y2[n] + (h/2)*k22));
        k32 = f2((x + (h/2)), (y1[n] + (h/2)*(k21)), (y2[n] + (h/2)*k22));
        k41 = f1((x + h), (y1[n] + h*k31), (y2[n] + h*k32));
        k42 = f2((x + h), (y1[n] + h*k31), (y2[n] + h*k32));
     
// Guardando o valor de y1(n+1) e y2(n+1) nos vetores correspondentes
        y1[n+1] = y1[n] + (h/6)*(k11 + 2*k21 + 2*k31 + k41);
        y2[n+1] = y2[n] + (h/6)*(k12 + 2*k22 + 2*k32 + k42);
        
        x += h;
        
// Imprime os demais valores calculados como proposto
        imprime_resultado(x, y1[n + 1], y2[n + 1]);
    }
}
